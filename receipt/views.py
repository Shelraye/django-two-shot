from django.shortcuts import redirect
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from receipt.models import Accounts, Expenses, Receipts


class AccountsCreateView(LoginRequiredMixin, CreateView):
    model = Accounts
    template_name = "accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_account")


class AccountsListView(LoginRequiredMixin, ListView):
    model = Accounts
    template_name = "accounts/list.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_account")


class ExpensesCreateView(LoginRequiredMixin, CreateView):
    model = Expenses
    template_name = "expense_categories/create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_expense")


class ExpensesListView(ListView):
    model = Expenses
    template_name = "expense_categories/list.html"

    def get_queryset(self):
        return Expenses.objects.filter(owner=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipts
    template_name = "receipt/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")


class ReceiptListView(ListView):
    model = Receipts
    template_name = "receipt/list.html"

    def get_queryset(self):
        return Receipts.objects.filter(purchaser=self.request.user)


# Create your views here.

# Create your views here.
# List views
# class ModelNameListView(ListView):
#     model = ModelName
#     template_name = "model_names/list.html"

# # Detail views
# class ModelNameDetailView(DetailView):
#     model = ModelName
#     template_name = "model_names/detail.html"

# Create views
# class ModelNameCreateView(CreateView):
#     model = ModelName
#     template_name = "model_names/create.html"
#     fields = ["model", "properties", "for", "your", "form"]

#     # Redirects to the detail page for the
#     # instance just created, assuming that
#     # the path name is registered
#     def get_success_url(self):
#         return reverse_lazy("model_name_detail", args=[self.object.id])

# Update views
# class ModelNameUpdateView(UpdateView):
#     model = ModelName
#     template_name = "model_names/update.html"
#     fields = ["model", "properties", "for", "your", "form"]

#     # Redirects to the detail page for the
#     # instance just created, assuming that
#     # the path name is registered
#     def get_success_url(self):
#         return reverse_lazy("model_name_detail", args=[self.object.id])

# Update views
# class ModelNameUpdateView(UpdateView):
#     model = ModelName
#     template_name = "model_names/update.html"
#     fields = ["model", "properties", "for", "your", "form"]

#     # Redirects to the detail page for the
#     # instance just created, assuming that
#     # the path name is registered
#     def get_success_url(self):
#         return reverse_lazy("model_name_detail", args=[self.object.id])
