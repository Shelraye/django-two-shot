from django.contrib import admin

from receipt.models import Accounts, Expenses, Receipts

# Register your models here.


class ReceiptsAdmin(admin.ModelAdmin):
    pass


class AccountsAdmin(admin.ModelAdmin):
    pass


class ExpensesAdmin(admin.ModelAdmin):
    pass


admin.site.register(Accounts, AccountsAdmin)
admin.site.register(Receipts, ReceiptsAdmin)
admin.site.register(Expenses, ExpensesAdmin)
