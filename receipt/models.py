from django.db import models
from django.conf import settings


class Expenses(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="expense",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.name)


class Accounts(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="account",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.name)


class Receipts(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=False,
        related_name="receipt",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "Expenses",
        null=True,
        related_name="receipt",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Accounts",
        null=True,
        related_name="receipt",
        on_delete=models.CASCADE,
    )
