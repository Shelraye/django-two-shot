from django.urls import path

from receipt.views import (
    ReceiptListView,
    ReceiptCreateView,
    AccountsListView,
    AccountsCreateView,
    ExpensesListView,
    ExpensesCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("accounts/list/", AccountsListView.as_view(), name="list_account"),
    path(
        "accounts/create/", AccountsCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/list/",
        ExpensesListView.as_view(),
        name="list_expense",
    ),
    path(
        "categories/create/",
        ExpensesCreateView.as_view(),
        name="create_expense",
    ),
]
